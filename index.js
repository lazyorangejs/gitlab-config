const plugins = [
  '@semantic-release/commit-analyzer',
  '@semantic-release/release-notes-generator',
  '@semantic-release/gitlab',
]

module.exports = {
  plugins: process.env.NPM_PUBLISH_ENABLED === 'true' ? [...plugins, '@semantic-release/npm'] : plugins
}
